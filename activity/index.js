let user1 = {
	username: "jackscepticeye",
	email: "seanScepticeye@gmail.com",
	age: 31,
	topics: ["Gaming", "Commentary"],
}

function isLegalAge(age) {
		if (user1.age > 18) {
			console.log("Welcome to the Club.")
		} else {console.log("You cannot enter the club yet.")}
	}

function addTopic(topic) {
	if (topic.length >= 5) {
		user1.topics.push(topic)
	} else {console.log("Enter a topic with at least 5 characters.")}
}

let clubMembers = ["jackscepticeye", "pewdiepie"];

function register(username) {
	if(username.length >= 8) {
		clubMembers.push(username);
	} else {"Please enter a username longer or equal to 8 characters."}
}


isLegalAge(user1);
addTopic("Math");
addTopic("Comedy");
console.log(user1.topics);
register("markiplier");
console.log(clubMembers);