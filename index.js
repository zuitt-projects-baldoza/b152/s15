console.log("Hello World from s15!");

let numString1 = "5";
let numString2 = "10";
let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;

// Operators
// Mathematical Operators (+,-,/,*,%)

console.log(num1+num2);

let sum1 = num1+num2;
console.log(sum1);

let numString3 = numString1 + sum1;
console.log(numString3);

let sampleStr = "Charles";
console.log(sampleStr+num2);

// Difference - returns a number we can save in a variable

let difference1 = num1 - num3;
console.log(difference1);

// string is converted back to number then subtracted

let difference2 = numString2 - num2;
console.log(difference2);

let sampleStr2 = "Joseph";

let difference3 = sampleStr2 - num1;
console.log(difference3);
// NaN - not a number. "Joseph" was converted to a number and resulted to NaN, NaN - number will result to NaN.

let difference4 = numString2 - numString1;
console.log(difference4);

// Mini Activity
function subtract(num1,num2){
	return num1 - num2;
}

let difference5 = subtract(num1,num2);
console.log(difference5);

// Multiplication Operator (*)
// Convert any string to number before multiplying

let product1 = num1*num2;
console.log(product1);

let product2 = numString1 * numString2;
console.log(product2);

// Division Operator (/)
// Convert to string before dividing

let quotient1 = num2/num4;
console.log(quotient1);

let quotient2 = numString2/numString1;
console.log(quotient2);

// Mini Activity

function multiply(num1,num2){
	return num1*num2;
}

let product3 = multiply(4,5);
console.log(product3);

function divide(num1,num2){
	return num1/num2;
}

let quotient3 = divide(20,5);
console.log(quotient3);

// Multiplying or Dividing to 0
console.log(product3*0);
console.log(quotient3/0);
// infinity - special type of number meaning we cannot divide a number by 0

// Modulo Operator (%)

console.log(5%5);
console.log(25%6);
console.log(50%2);
// 50/2 = 25

let modulo1 = 5%6;
console.log(modulo1);
// We were able to save value

// Basic Assignment Operator (=)
// Allows us to assign a value to the left operand.
// Allows us to assign an initial value to variable.

// leftOperand = rightOperand

let variable = "initial value";
variable = "new value";
console.log(variable);

let sample1 = "sample value";
variable = sample1;
console.log(variable);
console.log(sample1);

let sample2 = "new sample value"
variable = sample2;
console.log(variable);
console.log(sample2);

// constant's value cannot update or reassigned.
// const pi = 3.1416;
// pi = 5000;
// console.log(pi);

// don't add = to return statement

// Addition Assignment Operator (+=)
// 

let sum2 = 10;
sum2 += 20;
console.log(sum2);

let sum3 = 5
sum3 += 5;
console.log(sum3);

// When using addition assignment operator,
// Keep in mind that left had operand/left operand should be a variable.
// console.log(5+=5);

let sum4 = 30;
sum4+="Curry";
console.log(sum4);

let sum5 = "50";
sum5+="50";
console.log(sum5);

let fullName = "Wardell";
let name1 = "Stephen";

fullName+=name1;
console.log(fullName);
fullName+="Curry";
console.log(fullName);
fullName+="II";
console.log(fullName);

// Subtraction Assignment Operator (-=)
// Result of subtraction between both operands will be
// saved/re-assigned as the value of the leftOperand

let numSample = 50;
numSample-=10;
console.log(numSample);

let numberString = "100";
let numberString2 = "50";
numberString-=numberString2;
console.log(numberString);
// only left operands change

let text = "ChickenDinner";
text -= "Dinner";
console.log(text);
// NaN - not a number

// Multiplication Assignment Operator (*=)
// result of multiplication between operands will be
// re-assigned as the value of the left operand

let sampleNum1 = 3;
let sampleNum2 = 4;
sampleNum1*=sampleNum2;
console.log(sampleNum1);
console.log(sampleNum2);

let sampleNum3 = 5;
let sampleNum4 = "6";
sampleNum3*=sampleNum4;
console.log(sampleNum3);

// Division Assignment Operator(/=)
// result of division between both operands
// will be re-assigned as the value of the leftOperand

let sampleNum5 = 70;
let sampleNum6 = 10;
// sampleNum5 = 7
sampleNum5/=sampleNum6;
console.log(sampleNum5);

let sampleNum7 = 45;
sampleNum7/=0;
console.log(sampleNum7);

// Order of Operations follow (MDAS)

let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);
// MDAS - Multiplication, Division, Addition, Subtraction


let pemdasResult = 1 + (2-3)*(4/5);
console.log(pemdasResult);


// Increment and Decrement
// adding or subtracting 1 from a variable and then re-assigning the result to the variable.

// 2 implementation: pre-fix and post-fix

let z = 1;

// Pre-fix:

++z;
console.log(z); 
// The value of z was added with 1 and it
// is immediately returned. With pre-fix increment/decrement, the
// incremented/decremented value is returned at once.

// Post-Fix
console.log(z++);
console.log(z);
// With post-fix incrementation, we add 1 to the value of the
// variable, however, the difference is that the previous value is
// returned first before the incremented value.
z++;
console.log(z);

// Prefix vs Postfix
console.log(++z);
// 5 increment first and then returned the incremented value.
console.log(z++);
// 5 previous value is returned first before incrementation.
console.log(z);
// 6 The new value is now returned.

console.log(--z);

let sampleNumString = "5";
console.log(++sampleNumString);
// 6 - a numeric string is converted to number and will be incremented

let sampleString = "James";
console.log(sampleString++);

console.log(1 == 1);

let isSame = 55 == 55;
console.log(isSame);

console.log(1 == 1);
// true - loose equality operator prioritizes
// the sameness of the value because Loose Equality Operator
// actually enforces Forced Coercion or when the types of the 
// operands are automatically changed before the comparison. 
// String was converted to number. 1 == 1 = true.

console.log(0 == false);

let sampleConvert = Number(false);
console.log(sampleConvert);

let sampleConvert2 = "2500";
sampleConvert2 = Number(sampleConvert2);
console.log(sampleConvert2);
// 2500 - converted to number  type

console.log(1 == true);

console.log(5 == "5");

console.log(true == "true");
// false - false coercion, "true" string was converted to number
// but since it was a word/alphanumeric, it resulted to NaN/1 == Nan is false

console.log(false == "false");


// Strict Equality Operator

console.log(1=="1");
// In loose equality (==) this is true.

console.log(1==="1");
// false - checks sameness of both values and types of the operands.

console.log("james2000" === "James2000");
// false = j not equal to J

console.log(55 === "55");

// Loose Inequality Operator

console.log(1 != "1");
// false
// Foerced Coercion:
// both operands converted to number:
// string "1" is also converted to number.
// 1 != 1 = equal so: not inequal = false

// Loose Inequality Operator returns true if 
// the operands are NOT equal, it will false if operands are found to be equal
console.log("James" != "John");
// true - "James" is not equal "John"

console.log(5 != 55);
// true - 5 is NOT equal to 55

console.log(1500 != "5000");
// true
// Forced Coercion:
// 1500 will be converted to number
// "5000" will be converted to number
// 1500 != 5000
// It is NOT equal

console.log(true != "true");
// true
// forced coercion: true converted to 1
// 1 is NOT equal to Nan
// It is inequal

// Strict Inequality Operator
console.log(5 !== "5");
// true - operands have same value they have diff types

console.log(true !== "true");
// true - operands are inequal because they have diff types

// Equality Operators and Inequality Operators with Variables:

let nameStr1 = "Juan";
let nameStr2 = "Jack";
let numberSample = 50;
let numberSample2 = 60;
let numStr1 = "15";
let numStr2 = "25";

console.log(numStr1 == 50);
console.log(numStr1 == 15);
console.log(numStr2 == 25);
console.log(nameStr1 != "James");
console.log(numberSample !== "50");
console.log(numberSample != "50");
console.log(nameStr1 != nameStr1);
console.log(nameStr2 != "jack");

// Relational Comparison Operators
let price1 = 500;
let price2 = 700;
let price3 = 8000;
letnumStrSample = "5500"

// greater than (>)
console.log(price1 > price2)
console.log(price3 > price2)


// Logical operators
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let user1 = {
	username: "peterphoenix_1999",
	age: 28,
	level: 15,
	isGuildAdmin: false
}

let user2 = {
	username: "kingBrodie00",
	age: 13,
	level: 50,
	isGuildAdmin: true
}

// not ! - turns a boolean into the opposite value
// let isAdmin = false;
// let isRegistered = true;
// let isLegalAge = true;

console.log(!isRegistered);
console.log(!isAdmin);

// CONDITIONAL STATEMENTS
// A conditional statement is a key feature of a programming language.
// Allows to perform certain tasks based on a condition
// Sample Conditional Satetements:
	// Is it windy today? (WHat do we do?)
	// Is it Monday today? (What do we do?)


// If-Else Statements
// If statement will run a code block if the condition specified is true or results are true
// let user1 = {
// 	username: "peterphoenix_1999",
// 	age: 28,
// 	level: 15,
// 	isGuildAdmin: false
// }

// let user2 = {
// 	username: "kingBrodie00",
// 	age: 13,
// 	level: 50,
// 	isGuildAdmin: true
// }

// if(user1.age >= 18){
// 	alert("You are allowed to enter!");
// }

// code block of condition is run only when condition given is met or results to true
// else statement will be run if the given condition in the if statement is not met.
// if(true){
		// code block to run
	// } else {
		// code block to run if if condition is not met.
	// }

// if(user2.age >= 18){
// 	alert("You are allowed to enter.");
// } else {
// 	alert ("User2, you are not allowed to enter.")
// }

if(user1.level >= 20){
	console.log("User1 is not a noobie.")
} else {
	console.log("User1 is a noobie.")
}

if(user2.level >= 20){
	console.log("User2 is not a noobie.")
} else {
	console.log("User2 is not a noobie.")
}


// Mini Activity
if (user1.isGuildAmin === true){
	console.log("Welcome Back, Guild Admin.")
} else {
	console.log("You are not authorized to enter.")
}

if(user1.level >= 35){
	console.log("Hello, Knight!");
} else if (user1.level >= 25){
	console.log("Hello, Swordsman!");
} else if (user1.level >= 10) {
	console.log("Hello, Rookie!")
} else {
	console.log("Level out of range")
}

if(user2.level >= 35){
	console.log("Hello, Knight!");
} else if (user2.level >= 25){
	console.log("Hello, Swordsman!");
} else if (user2.level >= 10) {
	console.log("Hello, Rookie!")
} else {
	console.log("Level out of range")
}

// Logical Operators for If conditions:

let usernameInput1 = "nicoleIsAwesome100";
let passwordInput1 = "iamawesomenicole";

let usernameInput2 = "";
let passwordInput2 = null;

function register(username,password){

	if(username === "" || password === null){
		console.log("Please complete the form.")
	} else {
		console.log("Thank you for registering.")
	}
}

register(usernameInput1,passwordInput1);
register(usernameInput2,passwordInput1);

function requirementChecker(level,isGuildAdmin){
	if(level <= 25 && isGuildAdmin === false){
		console.log("Welcome to the Newbies Guild")
	} else if (level > 25) {
		console.log("You are too strong")
	} else if (isGuildAdmin === true) {
		console.log("You are a guild admin.")
	} 
}

requirementChecker(user1.level,user1.isGuildAdmin);
requirementChecker(user2.level,user2.isGuildAdmin);

let user3 = {
	username: "richieBillions",
	age:20,
	level:20,
	isGuildAdmin: true
}

requirementChecker(user3.level,user3.isGuildAdmin);

function addNum(num1,num2){

	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number type")
	} else {
		console.log("One or both of the arguments are not numbers.")
	}
}

addNum(5,10);
addNum("6",20);

// typeof - used for validating data type of variables or data
// It returns a string after evaluating the data type of the data/variable that comes after it

let str = "sample"
console.log(typeof str);
console.log(typeof 75);

// Mini-Activity 



function dayChecker(day){

// // toLowercase() method makes a string all small caps.
// day = day.toLowerCase()
// console.log(day)

// 	if(day === "sunday"){
// 		console.log("Today is Sunday; Wear White.")
// 	} else if (day === "monday") {
// 		console.log("Today is monday; Wear Blue.")
// 	} else if (day === "tuesday") {
// 		console.log("Today is tuesday; Wear Green.")
// 	} else if  (day === "wednesday") {
// 		console.log("Today is wednesday; Wear Purple.")
// 	} else if  (day === "thursday") {
// 		console.log("Today is thursday; Wear Brown.")
// 	} else if (day === "friday") {
// 		console.log("Today is friday; Wear Red.")
// 	} else if (day === "saturday") {
// 		console.log("Today is saturday; Wear Pink.")
// 	}
// }

// Switch is a conditional statement which be an alternative else-if,
// if-else, where the data being checked or evaluated is of an expected input

// Switch will compare your expression/condition to match with a case. Then the statement for that case will run.


// syntax:
// switch(expression/condition){
	// 	case:value:
	// 		statement;
	// 		break;
	// 	default:
	// 		statement;
	// 		break;
	// }


switch(day.toLowerCase()){
case "sunday":
	console.log("Today is " + day + "; Wear White");
	break;
case "monday":
	console.log("Today is " + day + "; Wear Blue");
	break;
case "tuesday":
	console.log("Today is " + day + "; Wear Green");
	break;
case "wednesday":
	console.log("Today is " + day + "; Wear Purple");
	break;
case "thursday":
	console.log("Today is " + day + "; Wear Brown");
	break;
case "friday":
	console.log("Today is " + day + "; Wear Red");
	break;
case "saturday":
	console.log("Today is " + day + "; Wear Pink");
	break;
default:
	console.log("Invalid Inpput. Enter a valid day of the week.");
}}

dayChecker("wednesday");

// = basic assignment operator
// == Loose Equality
// === Strict Equality

// A switch which display the power level of a selected member

let member = "Jeremiah";
// the break statement is used to terminate the execution of the switch after it found a match

switch(member){
	case "Eugene":
		console.log("Your power level is 20000");
		break;
	case "Dennis":
		console.log("Your power level is 15000");
		break;
	case "Vincent":
		console.log("Your power level is 14500");
		break;
	case "Jeremiah":
		console.log("Your power level is 10000");
		break;
	case "Alfred":
		console.log("Your power level is 8000");
		break;
	default:
		console.log("Invalid Input. Add a Ghost Fighter member.")
}

// Mini-Activity
function capitalChecker(country){
	switch(country){
		case "philippines":
		console.log("Manila");
		break;
		case "usa":
		console.log("Washington D.C.");
		break;
		case "japan":
		console.log("Tokyo");
		break;
		case "germany":
		console.log("Berlin");
		break;
		default:
		console.log("Input is out of range. Choose another country.")
	}
}

capitalChecker("usa")

// Ternary Operator
	// Conditional statement as if-else. However it was introduced to be
	// short hand way to write if-else.

	// Syntax
	// condition ? if-statement: else-statement



let superHero = "Batman"
superHero === "Batman" ? console.log("You are rich.") : console.log("Bruce Wayne is richer than you.");


// Ternary operators require an else statement
// superHero === "Superman" ? console.log("Hi, Clark!");

let currentRobin = "Tim Drake";
let isFirstRobin = currentRobin === "Dick Grayson" ? true : false
console.log(isFirstRobin);




